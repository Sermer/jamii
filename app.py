from flask import Flask
from flask import render_template, request
import psycopg2

app = Flask(__name__)
app.config["CACHE_TYPE"] = "null"


con = psycopg2.connect(dbname='blog', user='postgres',host='localhost',password='password')

cursor = con.cursor()

# define a route decorator
@app.route('/')
def index():
    # fetch all the posts from the db
    cursor.execute("SELECT * FROM articles")
    # loop the data in the html template , index.html
    query = cursor.fetchall()
    print(query)
    # to least all the db data
    return render_template('index.html', data=query)


@app.route('/post', methods=['GET','POST'])
def post():
    if request.method == 'POST':
   
        #define the post name in the html
        # use the request object to get the data from the html forms
        article = request.form.get('post')
        print(article)
        
        # save the data to the database
        cursor.execute("INSERT INTO articles (post) VALUES (%s)", (article,))
        con.commit()
        return render_template('index.html')
    else:
        return render_template('post.html')


@app.route('/viewpost')
def view_post():
    return render_template('single.html')

@app.route('/login')
def login():
    return render_template('login.html')    

@app.route('/users')
def list_users():
    cursor.execute("SELECT * FROM users")
    query = cursor.fetchall()
    print(query)
    return render_template('user_list.html', data=query)

@app.route('/register', methods=['GET','POST'])
def register():
    if request.method == 'POST':

        username = request.form.get('username')
        password = request.form.get('password')
        print(username)
        print(password)
        cursor.execute("INSERT INTO users (username, password) VALUES (%s, %s)",(username, password))
        con.commit()
        return render_template('login.html')
    else:
        # save to database
        return render_template('register.html')  

